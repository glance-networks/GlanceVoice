# Glance Voice SDK

The Glance mobile SDK provides an optional in-app calling capability to enable mobile app users to have a voice connection with contact center agents.  The Glance voice feature is available as an addition to the screen-sharing and agent video capabilities, which are core to the Mobile App Sharing SDK.  Glance voice is accessed by the Glance VoIP SDK, which supports both iOS and Android.  The VoIP SDK includes optional default user interface elements.

The typical VoIP SDK workflow is as follows:

1. A mobile user clicks on help button in mobile app.
2. The app initiates a call to a contact center mobile queue via the Glance VoIP SDK.
3. The agent picks up the call from queue.
4. The mobile user and agent are connected via voice call.
5. The agent initiates a mobile app sharing session.
6. The mobile user and agent connect via a screen-sharing session.
7. Both sessions end when customer or agent end session click the close button.

## Setup

### Cocoapods

Add the following line to the top of your Podfile:

    source 'https://gitlab.com/glance-networks/CocoaPodsSpecs'

Add the pod to your Podfile:

    pod 'GlanceVoice'

An example Podfile:

    source 'https://gitlab.com/glance-networks/CocoaPodsSpecs'

    target 'YourApp' do
        # Uncomment the next line if you're using Swift or would like to use dynamic frameworks
        use_frameworks!

        # Pods for YourApp
        pod 'GlanceVoice'
    end

## Usage

Import the GlanceVoice library.

### Objective-C

    @import GlanceVoice;

### Swift

    import GlanceVoice

## Support

Please contact `mobilesupport@glance.net` for questions or assistance.
